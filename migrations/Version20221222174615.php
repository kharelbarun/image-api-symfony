<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221222174615 extends AbstractMigration
{
	public function getDescription(): string
	{
		return '';
	}

	public function up(Schema $schema): void
	{
		// this up() migration is auto-generated, please modify it to your needs
		$this->addSql('CREATE TABLE image (id CHAR(36) NOT NULL, created_at DATETIME NOT NULL, filepath VARCHAR(255) NOT NULL, caption VARCHAR(255) NOT NULL, alt_text VARCHAR(255) NOT NULL, title_text VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
	}

	public function down(Schema $schema): void
	{
		// this down() migration is auto-generated, please modify it to your needs
		$this->addSql('DROP TABLE image');
	}
}
