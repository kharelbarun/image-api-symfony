<?php

namespace App\Command\Common;

use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

trait CreateUsernameHandlerTrait
{
	protected function createUsernameHandler(
		QuestionHelper $helper,
		InputInterface $input,
		OutputInterface $output
	) {
		return new UsernameHandler($helper, $input, $output);
	}
}
