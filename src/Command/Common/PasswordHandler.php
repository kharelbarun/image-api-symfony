<?php

/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

namespace App\Command\Common;

use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class PasswordHandler
{
	protected $helper;
	protected $input;
	protected $output;

	public function __construct(
		QuestionHelper $helper,
		InputInterface $input,
		OutputInterface $output
	) {
		$this->helper = $helper;
		$this->input = $input;
		$this->output = $output;
	}

	public function promptPassword(string $questionString)
	{
		do {
			$question = new Question($questionString);
			$question->setHidden(true);
			$question->setHiddenFallback(false);

			$password = $this->helper->ask($this->input, $this->output, $question);

			if (true !== ($message = $this->validate($password))) {
				$this->output->writeln($message);
				continue;
			}

			break;
		} while (true);

		return $password;
	}

	public function validate(?string $password)
	{
		if (is_null($password)) {
			return 'The provided password is not valid because it is empty or contains only whitespace characters.';
		}
		if (preg_match('/\s/', $password)) {
			return 'The provided password is not valid because it contains whitespace characters.';
		}

		return true;
	}
}
