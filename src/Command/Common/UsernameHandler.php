<?php

/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

namespace App\Command\Common;

use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class UsernameHandler
{
	protected $helper;
	protected $input;
	protected $output;

	public function __construct(
		QuestionHelper $helper,
		InputInterface $input,
		OutputInterface $output
	) {
		$this->helper = $helper;
		$this->input = $input;
		$this->output = $output;
	}

	public function promptUsername(string $questionString)
	{
		do {
			$question = new Question($questionString);

			$username = $this->helper->ask($this->input, $this->output, $question);

			if (true !== ($message = $this->validate($username))) {
				$this->output->writeln($message);
				continue;
			}

			break;
		} while (true);

		return $username;
	}

	public function validate(?string $username)
	{
		if (is_null($username) || (trim($username) === '')) {
			return 'Empty string is not valid username.';
		}

		return true;
	}
}
