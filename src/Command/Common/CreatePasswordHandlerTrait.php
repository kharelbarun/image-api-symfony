<?php

namespace App\Command\Common;

use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

trait CreatePasswordHandlerTrait
{
	protected function createPasswordHandler(
		QuestionHelper $helper,
		InputInterface $input,
		OutputInterface $output
	) {
		return new PasswordHandler($helper, $input, $output);
	}
}
