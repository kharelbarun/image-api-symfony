<?php

/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

namespace App\Command;

use App\Command\Common\CreatePasswordHandlerTrait;
use App\Command\Common\CreateUsernameHandlerTrait;
use App\Repository\UserRepository;
use App\Service\UserManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class ChangePassword extends Command
{
	use CreateUsernameHandlerTrait;
	use CreatePasswordHandlerTrait;

	protected static $defaultName = 'app:change-password';
	protected static $defaultDescription = 'Change password of a user';

	protected $userManager;
	protected $userPasswordHasher;
	protected $userRepository;

	public function __construct(
		UserManager $userManager,
		UserPasswordHasherInterface $userPasswordHasher,
		UserRepository $userRepository
	) {
		parent::__construct();
		$this->userManager = $userManager;
		$this->userPasswordHasher = $userPasswordHasher;
		$this->userRepository = $userRepository;
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		/** @var QuestionHelper */
		$helper = $this->getHelper('question');

		$usernameHandler = $this->createUsernameHandler($helper, $input, $output);

		do {
			$promptMessage = "Enter username of user:\n";
			$username = $usernameHandler->promptUsername($promptMessage);
			$user = $this->userRepository->findOneBy([
				'username' => $username,
			]);

			if ($user === null) {
				$output->writeln("The provided username does not match any user.");
				continue;
			}

			break;
		} while (true);

		$passwordHandler = $this->createPasswordHandler($helper, $input, $output);

		do {
			$question = sprintf("Enter new password for the user having username '%s':\n", $username);
			$newPassword = $passwordHandler->promptPassword($question);

			if ($this->userPasswordHasher->isPasswordValid($user, $newPassword)) {
				$output->writeln('The provided password is same as the current password.');
				continue;
			}

			break;
		} while (true);

		$this->userManager->changePassword($user, $newPassword);
		$output->writeln(sprintf("Password has been updated for user having username '%s'.", $username));

		return Command::SUCCESS;
	}
}
