<?php

/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

namespace App\Command;

use App\Command\Common\CreatePasswordHandlerTrait;
use App\Command\Common\CreateUsernameHandlerTrait;
use App\Repository\UserRepository;
use App\Service\UserManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetupUser extends Command
{
	use CreateUsernameHandlerTrait;
	use CreatePasswordHandlerTrait;

	protected static $defaultName = 'app:setup:user';
	protected static $defaultDescription = 'Setup user';

	protected $userManager;
	protected $userRepository;

	public function __construct(
		UserManager $userManager,
		UserRepository $userRepository
	) {
		parent::__construct();
		$this->userManager = $userManager;
		$this->userRepository = $userRepository;
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		/** @var QuestionHelper */
		$helper = $this->getHelper('question');

		$usernameHandler = $this->createUsernameHandler($helper, $input, $output);

		do {
			$promptMessage = "Enter username of new user:\n";
			$username = $usernameHandler->promptUsername($promptMessage);
			$user = $this->userRepository->findOneBy([
				'username' => $username,
			]);

			if ($user !== null) {
				$output->writeln("The provided username is already in use.");
				continue;
			}

			break;
		} while (true);

		$passwordHandler = $this->createPasswordHandler($helper, $input, $output);
		$promptMessage = "Enter password:\n";
		$password = $passwordHandler->promptPassword($promptMessage);

		$this->userManager->create($username, $password);
		$output->writeln(sprintf("A user has been created with username '%s'.", $username));

		return Command::SUCCESS;
	}
}
