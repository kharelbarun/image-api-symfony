<?php

/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

namespace App\Controller;

use App\Entity\Image;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ImageController extends AbstractController
{
	protected $entityManager;

	public function __construct(EntityManagerInterface $entityManager)
	{
		$this->entityManager = $entityManager;
	}

	protected function getRepository(): EntityRepository
	{
		return $this->entityManager->getRepository(Image::class);
	}

	public function __invoke(string $id, Request $request)
	{
		$image = $this->getRepository()->find($id);

		if ($image === null) {
			return new JsonResponse([
				'code' => 'NOT_FOUND',
			], Response::HTTP_NOT_FOUND);
		}

		return new JsonResponse([
			'id' => $image->getId(),
			'url' => $request->getSchemeAndHttpHost() . $request->getBasePath() . $image->getFilepath(),
			'caption' => $image->getCaption(),
			'alt_text' => $image->getAltText(),
			'title_text' => $image->getTitleText(),
		]);
	}
}
