<?php

/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

namespace App\Controller;

use App\Entity\Image;
use App\Service\AuthenticationHandler;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Uid\Uuid;

class ImageCreateController extends AbstractController
{
	protected $fileUploader;
	protected $entityManager;
	protected $authenticationHandler;

	public function __construct(
		FileUploader $fileUploader,
		EntityManagerInterface $entityManager,
		AuthenticationHandler $authenticationHandler
	) {
		$this->fileUploader = $fileUploader;
		$this->entityManager = $entityManager;
		$this->authenticationHandler = $authenticationHandler;
	}

	public function __invoke(Request $request): Response
	{
		if ($response = $this->authenticationHandler->authenticate($request)) {
			return $response;
		}

		$imageFile = $request->files->get('file');
		try {
			$uploadedImage = $this->fileUploader->uploadFromController($imageFile);
		} catch (BadRequestException $ex) {
			return new JsonResponse([
				'code' => 'BAD_REQUEST',
				'message' => $ex->getMessage(),
			], Response::HTTP_BAD_REQUEST);
		}

		$image = (new Image())
			->setId(Uuid::v4()->toRfc4122())
			->setFilepath('/upload/image/' . $uploadedImage)
			->setCaption($request->request->get('caption', ''))
			->setAltText($request->request->get('alt_text', ''))
			->setTitleText($request->request->get('title_text', ''))
			->setCreatedAt(new \DateTime())
		;

		$this->entityManager->persist($image);
		$this->entityManager->flush();

		return new JsonResponse([
			'new_resource_id' => $image->getId(),
		]);
	}
}
