<?php

/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

namespace App\Controller;

use App\Entity\Image;
use App\Service\AuthenticationHandler;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ImageDeleteController extends AbstractController
{
	protected $entityManager;
	protected $authenticationHandler;

	public function __construct(
		EntityManagerInterface $entityManager,
		AuthenticationHandler $authenticationHandler
	) {
		$this->entityManager = $entityManager;
		$this->authenticationHandler = $authenticationHandler;
	}

	protected function getRepository(): EntityRepository
	{
		return $this->entityManager->getRepository(Image::class);
	}

	public function __invoke(string $id, Request $request)
	{
		if ($response = $this->authenticationHandler->authenticate($request)) {
			return $response;
		}

		/** @var Image $image */
		$image = $this->getRepository()->find($id);

		if ($image === null) {
			return new JsonResponse([
				'code' => 'NOT_FOUND',
			], Response::HTTP_NOT_FOUND);
		}

		$this->entityManager->remove($image);
		$this->entityManager->flush();

		$this->remove($image);

		return new JsonResponse([
			'code' => 'DELETED',
		]);
	}

	public function remove(Image $image): void
	{
		if ($image->getFilepath() === '') {
			return;
		}
		$filesystem = new Filesystem();
		$absolutePath = $this->getParameter('kernel.project_dir').'/public'.$image->getFilepath();
		$filesystem->remove($absolutePath);
	}
}
