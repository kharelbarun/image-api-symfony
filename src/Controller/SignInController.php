<?php

/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

namespace App\Controller;

use App\Repository\UserRepository;
use App\Service\AuthenticationHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class SignInController extends AbstractController
{
	protected $userRepository;

	protected $authenticationHandler;

	protected $userPasswordHasher;

	public function __construct(
		UserRepository $userRepository,
		AuthenticationHandler $authenticationHandler,
		UserPasswordHasherInterface $userPasswordHasher
	) {
		$this->userRepository = $userRepository;
		$this->authenticationHandler = $authenticationHandler;
		$this->userPasswordHasher = $userPasswordHasher;
	}

	public function __invoke(Request $request): Response
	{
		$requestData = $request->toArray();
		$username = $requestData['username'] ?? null;
		$password = $requestData['password'] ?? null;

		$user = $this->userRepository->findOneBy([
			'username' => $username,
		]);

		if ($user === null) {
			return new JsonResponse([
				'code' => 'SIGN_IN::USER_NOT_FOUND',
				'message' => 'Supplied username does not belong to any user.',
			], Response::HTTP_BAD_REQUEST);
		}

		if (! $this->userPasswordHasher->isPasswordValid($user, $password)) {
			return new JsonResponse([
				'code' => 'SIGN_IN::PASSWORD_MISMATCH',
				'message' => 'Supplied password is not the password of user.',
			], Response::HTTP_BAD_REQUEST);
		}

		$token = $this->authenticationHandler->generateToken($user);

		return new JsonResponse([
			'code' => 'SIGN_IN::OK',
			'auth_token' => $token,
		]);
	}
}
