<?php

/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

namespace App\Controller;

use App\Entity\Image;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ImageIndexController extends AbstractController
{
	private $entityManager;

	public function __construct(
		EntityManagerInterface $entityManager
	) {
		$this->entityManager = $entityManager;
	}

	public function __invoke(Request $request): Response
	{
		$repository = $this->entityManager->getRepository(Image::class);
		$limit = 10;
		$images = $repository->findBy([], [ 'createdAt' => 'DESC' ], $limit);
		$responseData = array_map(function (Image $image) use ($request) {
			return [
				'id' => $image->getId(),
				'url' => $request->getSchemeAndHttpHost() . $request->getBasePath() . $image->getFilepath(),
				'caption' => $image->getCaption(),
				'alt_text' => $image->getAltText(),
				'title_text' => $image->getTitleText(),
			];
		}, $images);

		return new JsonResponse($responseData);
	}
}
