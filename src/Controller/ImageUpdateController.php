<?php

/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

namespace App\Controller;

use App\Entity\Image;
use App\Service\AuthenticationHandler;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ImageUpdateController extends AbstractController
{
	protected $entityManager;

	protected $authenticationHandler;

	public function __construct(
		EntityManagerInterface $entityManager,
		AuthenticationHandler $authenticationHandler
	) {
		$this->entityManager = $entityManager;
		$this->authenticationHandler = $authenticationHandler;
	}

	protected function getRepository(): EntityRepository
	{
		return $this->entityManager->getRepository(Image::class);
	}

	public function __invoke(string $id, Request $request): Response
	{
		if ($response = $this->authenticationHandler->authenticate($request)) {
			return $response;
		}

		/** @var Image $image */
		$image = $this->getRepository()->find($id);

		if ($image === null) {
			return new JsonResponse([
				'code' => 'NOT_FOUND',
			], Response::HTTP_NOT_FOUND);
		}

		$requestData = $request->toArray();

		if ($caption = $requestData['caption'] ?? null) {
			$image->setCaption($caption);
		}
		if ($altText = $requestData['alt_text'] ?? null) {
			$image->setAltText($altText);
		}
		if ($titleText = $requestData['title_text'] ?? null) {
			$image->setTitleText($titleText);
		}

		$this->entityManager->flush();

		return new JsonResponse([
			'code' => 'UPDATED',
		]);
	}
}
