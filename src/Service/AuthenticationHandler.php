<?php

/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

namespace App\Service;

use App\Entity\User;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Firebase\JWT\SignatureInvalidException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthenticationHandler
{
	protected $jwtKey;

	public function __construct(
		string $jwtKey
	) {
		$this->jwtKey = $jwtKey;
	}

	public function generateToken(User $user): string
	{
		$now = new \DateTimeImmutable();
		return JWT::encode([
			'iat' => $now->getTimestamp(),
			'exp' => $now->modify('+24hours')->getTimestamp(),
			'user' => $user->getId(),
		], $this->jwtKey, 'HS256');
	}

	public function authenticate(Request $request, &$jwt = null, &$jwtDecoded = null): ?Response
	{
		$authorization = $request->headers->get('Authorization');
		if (! $authorization) {
			return new JsonResponse([
				'code' => 'UNAUTHORIZED',
				'message' => 'No "Authorization" header found.',
			], Response::HTTP_UNAUTHORIZED);
		}
		if (! preg_match('/^Bearer (?<token>.+)/', $authorization, $matches)) {
			return new JsonResponse([
				'code' => 'UNAUTHORIZED',
				'message' => 'Authorization header signature mismatch. Expected: \'Bearer <token>\'.',
			], Response::HTTP_UNAUTHORIZED);
		}
		$jwt = $matches['token'];
		try {
			$jwtDecoded = JWT::decode($jwt, new Key($this->jwtKey, 'HS256'));
		} catch (
			\InvalidArgumentException|\DomainException|\UnexpectedValueException
			|SignatureInvalidException|BeforeValidException|ExpiredException
			$exception
		) {
			return new JsonResponse([
				'code' => 'UNAUTHORIZED',
				'message' => $exception->getMessage(),
			], Response::HTTP_UNAUTHORIZED);
		}

		return null;
	}
}
