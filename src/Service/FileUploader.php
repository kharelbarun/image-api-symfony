<?php

/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

namespace App\Service;

use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\File\Exception\IniSizeFileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
	private $targetDirectory;

	public function __construct(
		string $targetDirectory
	) {
		$this->targetDirectory = $targetDirectory;
	}

	public function upload(UploadedFile $file): string
	{
		$originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
		$fileName = $originalFilename.'-'.uniqid().'.'.$file->getClientOriginalExtension();

		$file->move($this->targetDirectory, $fileName);

		return $fileName;
	}

	public function uploadFromController(UploadedFile $file)
	{
		try {
			return $this->upload($file);
		} catch (IniSizeFileException $ex) {
			throw new BadRequestException($ex->getMessage());
		}
	}
}
