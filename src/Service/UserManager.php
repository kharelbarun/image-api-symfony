<?php

/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserManager
{
	protected $entityManager;

	protected $userPasswordHasher;

	public function __construct(
		EntityManagerInterface $entityManager,
		UserPasswordHasherInterface $userPasswordHasher
	) {
		$this->entityManager = $entityManager;
		$this->userPasswordHasher = $userPasswordHasher;
	}

	public function create(string $username, string $password)
	{
		$user = (new User())
			->setUsername($username)
			->setPassword($password)
		;
		$hashedPassword = $this->userPasswordHasher->hashPassword($user, $password);

		$user->setPassword($hashedPassword);

		$this->entityManager->persist($user);
		$this->entityManager->flush();
	}

	public function changePassword(User $user, $newPassword)
	{
		$hashedPassword = $this->userPasswordHasher->hashPassword($user, $newPassword);

		$user->setPassword($hashedPassword);

		$this->entityManager->flush();
	}
}
