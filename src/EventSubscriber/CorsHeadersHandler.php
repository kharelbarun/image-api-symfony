<?php

/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class CorsHeadersHandler implements EventSubscriberInterface
{
	private $corsAllowOrigins;

	public function __construct(array $corsAllowOrigins)
	{
		$this->corsAllowOrigins = $corsAllowOrigins;
	}

	public static function getSubscribedEvents(): array
	{
		return [
			KernelEvents::REQUEST => 'handleKernelRequest',
			KernelEvents::RESPONSE => 'handleKernelResponse',
		];
	}

	public function handleKernelRequest(RequestEvent $event)
	{
		// $event->getRequest()->getMethod() does not return 'OPTIONS'
		// TODO: do not use global $_SERVER
		if ($_SERVER['REQUEST_METHOD'] !== Request::METHOD_OPTIONS) {
			return;
		}

		$response = new Response('');
		$this->setCorsHeaders($event->getRequest(), $response);

		// response is sent immediately otherwise 403 is responded even if 200 is returned
		// TODO: do not call 'exit'
		$response->send();
		exit;

		// $event->setResponse($response);
	}

	public function handleKernelResponse(ResponseEvent $event)
	{
		$this->setCorsHeaders($event->getRequest(), $event->getResponse());
	}

	protected function setCorsHeaders(Request $request, Response $response)
	{
		// Due to header "Access-Control-Allow-Credentials: true"
		// some headers like Access-Control-Allow-Origin and Access-Control-Allow-Headers
		// are not allowed to have "*" as value

		$requestOrigin = $request->headers->get('Origin');
		foreach ([
			'Access-Control-Allow-Origin' => in_array($requestOrigin, $this->corsAllowOrigins) ? $requestOrigin : '',
			'Access-Control-Allow-Headers' => 'Authorization,Content-Type',
			'Access-Control-Allow-Methods' => 'GET,POST,PUT,DELETE,OPTIONS',
			// 'Access-Control-Allow-Credentials' => 'true',
			'Access-Control-Max-Age' => '86400',
		] as $headerKey => $headerValue) {
			$response->headers->set($headerKey, $headerValue);
		}
	}
}
