<?php

/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Image
{
	/**
	 * @var string
	 * @ORM\Column(type="string", length=36, options={"fixed"=true})
	 * @ORM\Id
	 */
	private $id;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 */
	private $filepath;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 */
	private $caption = '';

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 */
	private $altText = '';

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 */
	private $titleText = '';

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 */
	private $createdAt;

	public function getId(): string
	{
		return $this->id;
	}

	public function setId(string $id): self
	{
		$this->id = $id;
		return $this;
	}

	public function getFilepath(): string
	{
		return $this->filepath;
	}

	public function setFilepath(string $filepath): self
	{
		$this->filepath = $filepath;
		return $this;
	}

	public function getCaption(): string
	{
		return $this->caption;
	}

	public function setCaption(string $caption): self
	{
		$this->caption = $caption;
		return $this;
	}

	public function getAltText(): string
	{
		return $this->altText;
	}

	public function setAltText(string $altText): self
	{
		$this->altText = $altText;
		return $this;
	}

	public function getTitleText(): string
	{
		return $this->titleText;
	}

	public function setTitleText(string $titleText): self
	{
		$this->titleText = $titleText;
		return $this;
	}

	public function getCreatedAt(): \DateTime
	{
		return $this->createdAt;
	}

	public function setCreatedAt(\DateTime $createdAt): self
	{
		$this->createdAt = $createdAt;
		return $this;
	}
}
